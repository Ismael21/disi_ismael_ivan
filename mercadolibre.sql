-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-12-2020 a las 21:39:45
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mercadolibre`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `idarticulo` int(11) NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `precio` int(11) NOT NULL,
  `tamaño` varchar(25) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `estatus` varchar(15) NOT NULL,
  `entrega` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`idarticulo`, `nombre`, `descripcion`, `precio`, `tamaño`, `imagen`, `estatus`, `entrega`) VALUES
(1, 'Coca Cola', 'Refresco', 15, '12x33cm', 'src/imagenes/logoM.png', 'Habilitado', 'No se entrega el dia siguiente'),
(2, 'Xbox', 'one', 14000, '33x44cm', 'xbox.image', 'habilitado', 'si'),
(3, 'Zuko', 'Sabor fressa', 5, '10x5cm', 'C:\\Users\\Jovan Ivan Castañeda\\Documents\\cross.png', 'Habilitado', 'Se entrega el dia siguiente'),
(4, 'Sello Rojo', 'Leche', 12, '12x10cm', 'C:\\Users\\Jovan Ivan Castañeda\\Pictures\\9a552a4e-1470-4ebd-b90a-17c6b200e927.gif', 'Desabilitado', 'Se entrega el dia siguiente'),
(5, 'Nike', 'Zapatos', 369, 'Talla 27', 'C:\\Users\\Jovan Ivan Castañeda\\Pictures\\1539304924_d0o107bmuck11.gif', 'Habilitado', 'Se entrega el dia siguiente'),
(6, 'Samsung', 'Telefono celular', 2000, '12p', 'C:\\Users\\Jovan Ivan Castañeda\\Documents\\phone.png', 'Habilitado', 'Se entrega el dia siguiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auxproductoventa`
--

CREATE TABLE `auxproductoventa` (
  `idarticulo` int(11) NOT NULL,
  `idventa` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auxproductoventa`
--

INSERT INTO `auxproductoventa` (`idarticulo`, `idventa`, `cantidad`, `precio`) VALUES
(1, 1, 15, 225),
(5, 1, 2, 738),
(3, 2, 34, 170),
(1, 2, 34, 510),
(5, 2, 2, 738);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `idEstado` int(11) NOT NULL,
  `nombreE` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`idEstado`, `nombreE`) VALUES
(1, 'Aguascalientes'),
(2, 'Baja California'),
(3, 'Baja Clifornia Sur'),
(4, 'Campeche'),
(5, 'Chihuahua'),
(6, 'Chiapas'),
(7, 'Cuidad De Mexico'),
(8, 'Coahuila'),
(9, 'Colima'),
(10, 'Durango'),
(11, 'Guanajuato'),
(12, 'Guerrero'),
(13, 'Hidalgo'),
(14, 'Jalisco'),
(15, 'Mexico'),
(16, 'Michoacan'),
(17, 'Morelos'),
(18, 'Nayarit'),
(19, 'Nuevo Leon'),
(20, 'Oaxaca'),
(21, 'Puebla'),
(22, 'Queretaro'),
(23, 'Quintana Roo'),
(24, 'San Luis Potosi'),
(25, 'Sinaloa'),
(26, 'Sonora'),
(27, 'Tabasco'),
(28, 'Tamaulipas'),
(29, 'Tlaxcala'),
(30, 'Veracruz'),
(31, 'Yucatan'),
(32, 'Zacatecas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `idpaises` int(11) NOT NULL,
  `nombreP` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`idpaises`, `nombreP`) VALUES
(1, 'MEXICO'),
(2, 'EUA'),
(3, 'CANADÃ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

CREATE TABLE `privilegio` (
  `idprivilegio` int(11) NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `contra` varchar(25) NOT NULL,
  `rol` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `privilegio`
--

INSERT INTO `privilegio` (`idprivilegio`, `nombre`, `contra`, `rol`) VALUES
(1, 'admin', 'admin', 'administrador'),
(2, 'Aizen', 'bleach', 'vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `calle` varchar(50) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `colonia` varchar(50) NOT NULL,
  `municipio` varchar(50) NOT NULL,
  `pais` varchar(25) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `correo` varchar(40) NOT NULL,
  `Estatus` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `calle`, `cp`, `numero`, `colonia`, `municipio`, `pais`, `imagen`, `correo`, `Estatus`) VALUES
(1, 'Aizen Sosuke', 'Roma', '43943', '3238348432', 'Alejandria', 'Jalisco', 'MEXICO', 'C:\\Users\\Jovan Ivan Castañeda\\Pictures\\Pixel-Art-de-los-personajes-de-Super-Smash-Bros_06.png', 'aizen369@gmail.com', 'Alta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `Idventa` int(11) NOT NULL,
  `Idusuario` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `total_venta` int(11) NOT NULL,
  `estatus` varchar(45) DEFAULT NULL,
  `pago` int(11) DEFAULT NULL,
  `cambio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`Idventa`, `Idusuario`, `fecha`, `total_venta`, `estatus`, `pago`, `cambio`) VALUES
(1, 1, '2020-12-11', 963, 'Realizada', 1000, -37),
(2, 1, '2020-12-11', 1418, 'Cancelada', 2000, -582);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`idarticulo`);

--
-- Indices de la tabla `auxproductoventa`
--
ALTER TABLE `auxproductoventa`
  ADD KEY `idventa_idx` (`idventa`),
  ADD KEY `idarticulo_idx` (`idarticulo`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`idpaises`);

--
-- Indices de la tabla `privilegio`
--
ALTER TABLE `privilegio`
  ADD PRIMARY KEY (`idprivilegio`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`Idventa`),
  ADD KEY `idusuario_idx` (`Idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `idarticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `idpaises` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `privilegio`
--
ALTER TABLE `privilegio`
  MODIFY `idprivilegio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `Idventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auxproductoventa`
--
ALTER TABLE `auxproductoventa`
  ADD CONSTRAINT `idarticulo` FOREIGN KEY (`idarticulo`) REFERENCES `articulos` (`idarticulo`),
  ADD CONSTRAINT `idventa` FOREIGN KEY (`idventa`) REFERENCES `ventas` (`Idventa`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `idusuario` FOREIGN KEY (`Idusuario`) REFERENCES `usuarios` (`idusuarios`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
