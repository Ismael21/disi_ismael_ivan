-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2020 a las 02:49:10
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mercado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `idarticulo` int(11) NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `precio` int(11) NOT NULL,
  `tamaño` varchar(25) NOT NULL,
  `imagen` varchar(50) NOT NULL,
  `estatus` varchar(15) NOT NULL,
  `entrega` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`idarticulo`, `nombre`, `descripcion`, `precio`, `tamaño`, `imagen`, `estatus`, `entrega`) VALUES
(2, 'XBOX SERIES X', '12 teraflops', 12499, ' 6,5 x 15,1 x 27,5 cm.', 'D:/imagenes/xbox/', ' habilitado', 'mañana'),
(3, 'CELULAR', 'ips 6 gb ram + 64 gb interno, FHD', 10000, '5 x 12 x 2 cm', 'D:/imagenes/\ntelefono/', 'habilitado', 'hoy'),
(4, 'SALA', 'cubierta de cuero', 9699, '4 x 2 x 2 mts', 'D:/imagenes/sala/', 'desabilitado', 'mañana'),
(5, 'ESCRITORIO', 'Superficie de madera', 7500, '1.2 x 1.5 x 0.8 mts', 'D:/imagenes/\nescritorio/', 'habilitado', 'hoy'),
(6, 'COMEDOR', 'Superficie de madera', 8399, '2.2 x 2.5 x 0.8 mts', 'D:/imagenes/comedor/', 'habilitado', 'mañana'),
(7, 'PC', 'Xtreme Pc Gamer Geforce Gtx 1660 Super Ryzen 5 16gb Ssd 1tb', 22899, '47 x 20 \nx 48 cm', 'D:/imagenes/pc/', 'habilitado', 'hoy'),
(8, 'BALON', 'para futbool', 120, ' 21,65 x 22,29 cm', 'D:/imagenes/balon/', 'inabilitado', 'mañana'),
(9, 'PLAYERA', 'para hombre, grande color azul', 150, '41 x 50 cm', 'D:/imagenes/playera/', 'habilitado', 'hoy'),
(10, 'BICICLETA', 'especial para montaña', 1899, '0.7 x 1.3 x 0.5 mts', 'D:/imagenes/bisicleta/', 'disponible', 'mañana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auxproductoventa`
--

CREATE TABLE `auxproductoventa` (
  `idarticulo` int(11) NOT NULL,
  `idventa` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `idEstado` int(11) NOT NULL,
  `nombreE` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`idEstado`, `nombreE`) VALUES
(1, 'Aguascalientes'),
(2, 'Baja California'),
(3, 'Baja Clifornia Sur'),
(4, 'Campeche'),
(5, 'Chihuahua'),
(6, 'Chiapas'),
(7, 'Cuidad De Mexico'),
(8, 'Coahuila'),
(9, 'Colima'),
(10, 'Durango'),
(11, 'Guanajuato'),
(12, 'Guerrero'),
(13, 'Hidalgo'),
(14, 'Jalisco'),
(15, 'Mexico'),
(16, 'Michoacan'),
(17, 'Morelos'),
(18, 'Nayarit'),
(19, 'Nuevo Leon'),
(20, 'Oaxaca'),
(21, 'Puebla'),
(22, 'Queretaro'),
(23, 'Quintana Roo'),
(24, 'San Luis Potosi'),
(25, 'Sinaloa'),
(26, 'Sonora'),
(27, 'Tabasco'),
(28, 'Tamaulipas'),
(29, 'Tlaxcala'),
(30, 'Veracruz'),
(31, 'Yucatan'),
(32, 'Zacatecas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `idpaises` int(11) NOT NULL,
  `nombreP` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`idpaises`, `nombreP`) VALUES
(1, 'MEXICO'),
(2, 'EUA'),
(3, 'CANADÁ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `calle` varchar(50) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `colonia` varchar(50) NOT NULL,
  `estado` varchar(150) NOT NULL,
  `pais` varchar(45) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `Estatus` varchar(4) NOT NULL DEFAULT 'Alta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `calle`, `cp`, `numero`, `colonia`, `estado`, `pais`, `imagen`, `correo`, `Estatus`) VALUES
(1, 'Alex', 'maiz', '46404', '12', 'mezcalera', '', '', '', '', 'Alta'),
(2, 'Ismael', 'chevere', '77765', '71', 'chola', '', '', '', '', 'Alta'),
(3, 'Alex', 'Calle', '32323', '123', 'col.', 'Aguascalientes', 'MEXICO', 'jiui9u8', 'u98u899u9', 'Alta'),
(4, 'Julian', 'ejido', '90120', '768', 'Texcoco', 'San Luis Potosi', 'MEXICO', 'C:\\Users\\Ismael\\OneDrive\\Documentos\\Captura 2.PNG', 'gnomio@gmail.com', 'Baja'),
(5, 'Alfreo', 'Avenida', '78998', '9', 'Obrera', 'Tamaulipas', 'MEXICO', 'C:\\User\\images', 'alf@hotmail.com', 'Baja'),
(6, 'Ajeandro', 'Hidalgo', '98000', '89', 'Centro', 'Aguascalientes', 'MEXICO', 'D:\\imagenes', 'jiojoimijij.@hotmail.com', 'Baja'),
(7, '', '', '', '', '', 'Aguascalientes', 'MEXICO', '', 'jjijjoi.@hotmail.com', 'Alta'),
(8, '', '', '', '', '', 'Aguascalientes', 'MEXICO', '', 'kjkjkjj789789___---..}}}{+´+´+´+', 'Alta'),
(9, 'Alexis', 'maiz', '46404', '12', 'mezcalera', 'Jalisco', 'MEXICO', 'lol.png', 'isod0123isao@gmail.com', 'Baja'),
(10, 'Hugo', '14 de Mayo', '09888', '10', 'Mezcalera', 'Mexico', 'MEXICO', 'C:\\Users\\Ismael\\Pictures\\Saved Pictures\\Foto del teclado de un piano 1.png', 'hcom', 'Baja');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idventa` int(11) NOT NULL,
  `Idusuario` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `total_venta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idventa`, `Idusuario`, `fecha`, `total_venta`) VALUES
(1, 1, '2012-12-12', 12000),
(2, 2, '2018-12-14', 1200),
(3, 3, '2015-12-20', 102),
(4, 4, '2010-08-12', 890),
(5, 5, '2020-12-01', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`idarticulo`);

--
-- Indices de la tabla `auxproductoventa`
--
ALTER TABLE `auxproductoventa`
  ADD KEY `idarticulo_idx` (`idarticulo`),
  ADD KEY `idventa_idx` (`idventa`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`idpaises`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idventa`),
  ADD KEY `idusuario_idx` (`Idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `idarticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `idpaises` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auxproductoventa`
--
ALTER TABLE `auxproductoventa`
  ADD CONSTRAINT `idarticulo` FOREIGN KEY (`idarticulo`) REFERENCES `articulos` (`idarticulo`),
  ADD CONSTRAINT `idventa` FOREIGN KEY (`idventa`) REFERENCES `venta` (`idventa`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `idusuario` FOREIGN KEY (`Idusuario`) REFERENCES `usuario` (`idusuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
