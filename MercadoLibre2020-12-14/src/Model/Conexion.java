/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Jovan Ivan Castañeda
 */
public class Conexion {
    public final String server="jdbc:mysql://127.0.0.1:3306/mercadolibre?serverTimezone=UTC";
    public final String user="root";
    public final String pass="";
    public Connection cone;
    
    public Conexion(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            cone=DriverManager.getConnection(server, user, pass);
            System.out.println("Conecion Exitosa");
        } catch (Exception e) {
            System.out.println("Error al conectarce");
            System.out.println(e.toString());
        }
    }
}
