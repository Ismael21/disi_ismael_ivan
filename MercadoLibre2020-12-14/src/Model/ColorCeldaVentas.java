
package Model;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Ismael
 */
public class ColorCeldaVentas extends DefaultTableCellRenderer {
    
 
    
    @Override
    public Component getTableCellRendererComponent(JTable tabla, Object value, boolean Selected, boolean hasFocus, int row, int col){
        super.getTableCellRendererComponent(tabla, value, Selected, hasFocus, row, col);
        /*if(tabla.getValueAt(row, 7).toString().equals("Desabilitado")){
            setForeground(Color.RED);
            
        }else if(tabla.getValueAt(row, 7).toString().equals("Habilitado")) 
            setForeground(Color.BLACK);
*/
        if(row % 2 == 0){
            Font g= new Font("Tahoma",Font.PLAIN,11);
            tabla.setFont(g);
            setBackground(new Color(249, 119, 0));                                   
        }else{
             Font g= new Font("Tahoma",Font.PLAIN,11);
            tabla.setFont(g);
            setBackground(new Color(251, 198, 126));
        }                                     
       
        return this;
        
    
}
}
