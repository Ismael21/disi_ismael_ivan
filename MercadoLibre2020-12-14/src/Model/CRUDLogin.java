package Model;

import Controller.LoginController;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class CRUDLogin extends Conexion{
    
    /**
     * Metodo para agregar un nuevo usuario para el login
     * @param nombre -> nombre del usuario
     * @param contra -> contraseña del usuario
     * @param rol -> tipo de rol ya sea admin o vendedor
     * @return True -> si se inserto correctamente
     *         False -> si no se inserto
     */
    public boolean agregarPrivilegio(String nombre, String contra, String rol){
        try {
            String sentencia="INSERT INTO mercadolibre.privilegio values(null,?,?,?);";
            PreparedStatement stm=cone.prepareStatement(sentencia);
            stm.setString(1, nombre);
            stm.setString(2, contra);
            stm.setString(3, rol);
            stm.executeUpdate();
            stm.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error:"+e);
            return false;
        }
    }
    
    
    public String comprobarRol(String nombre, char[] contra){
        String cadena = String.valueOf(contra);
        String sentencia="SELECT * FROM mercadolibre.privilegio";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            rs=ps.executeQuery();
            while(rs.next()){
                if (nombre.equals(rs.getString(2))) {
                    System.out.println("Entro xxx");
                    System.out.println(contra);
                    if (cadena.equals(rs.getString(3))) {
                        System.out.println("Lo que retorna si es correcto "+ rs.getString(4));
                        return rs.getString(4);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return "";
    }
}
