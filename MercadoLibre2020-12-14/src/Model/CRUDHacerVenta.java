/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Jovan Ivan Castañeda
 */
public class CRUDHacerVenta extends Conexion{
    
    /**
     * Metodo para ver los productos habilitados
     * @return -> un array con los datos de la sentencia
     */
    public String[][] verVentas(){
        String datos[][]=null;
        String sentencia="SELECT idarticulo, nombre, descripcion, precio, tamaño, imagen, entrega, estatus FROM mercadolibre.articulos";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            rs=ps.executeQuery();
            int renglones = 0;
            while(rs.next()){
                renglones++;
                /*System.out.print("ID: "+rs.getString(1));
                System.out.print("  Nombre: "+rs.getString(2));
                System.out.print("  Contraseña: "+rs.getString(3));
                System.out.println("");*/
            }
            datos = new String[renglones][8];
                rs.first();
            int i = 0;
            do {                
                datos[i][0]=rs.getString(1);
                datos[i][1]=rs.getString(2);
                datos[i][2]=rs.getString(3);
                datos[i][3]=rs.getString(4);
                datos[i][4]=rs.getString(5);
                datos[i][5]=rs.getString(6);
                datos[i][6]=rs.getString(7);
                datos[i][7]=rs.getString(8);
                i++;
            } while (rs.next());
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
    
    /**
     * Metodo para ver los productos habilitados con algun parametro
     * @param Nombre -> parametro de busqueda
     * @return datos -> un array con los datos obtenidos de la sentencia
     */
    public String[][] getVentasLike(String Nombre){
        String datos[][]=null;
        String sentencia="SELECT idarticulo, nombre, descripcion, precio, tamaño, imagen, entrega FROM mercadolibre.articulos WHERE Nombre LIKE ?";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, Nombre);
            rs=ps.executeQuery();
            int renglones = 0;
            while(rs.next()){
                renglones++;
                /*System.out.print("ID: "+rs.getString(1));
                System.out.print("  Nombre: "+rs.getString(2));
                System.out.print("  Contraseña: "+rs.getString(3));
                System.out.println("");*/
            }
            datos = new String[renglones][8];
                rs.first();
            int i = 0;
            do {                
                datos[i][0]=rs.getString(1);
                datos[i][1]=rs.getString(2);
                datos[i][2]=rs.getString(3);
                datos[i][3]=rs.getString(4);
                datos[i][4]=rs.getString(5);
                datos[i][5]=rs.getString(6);
                datos[i][6]=rs.getString(7);
                i++;
            } while (rs.next());
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
    
    /**
     * Metodo para agregar la venta
     * @param idusuario -> usuario al que se le hara la venta
     * @param fecha -> fecha de la venta
     * @param total -> Costo total de la venta
     * @param pago
     * @param cambio
     * @return true -> si se inserto
     *         false -> no se inserto
     */
    public boolean agregarVenta(String idusuario, String fecha, String total, String pago, String cambio){
        String estatus = "Realizada";
        try {
            String sentencia="INSERT INTO mercadolibre.ventas values(null,?,?,?,?,?,?);";
            PreparedStatement stm=cone.prepareStatement(sentencia);
            stm.setString(1, idusuario);
            stm.setString(2, fecha);
            stm.setString(3, total);
            stm.setString(4, estatus);
            stm.setString(5, pago);
            stm.setString(6, cambio);
            stm.executeUpdate();
            stm.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error: "+e);
            return false;
        }
    }
    
    /**
     * Metodo para ver ver la ultima venta agregada
     * @return -> un String donde devuelve el id de la venta
     */
    public String verVentasId(){
        String datos = "";
        String sentencia="SELECT idventa FROM mercadolibre.ventas";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            rs=ps.executeQuery();
            while(rs.next()){
                datos = rs.getString(1);
            }
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
    
    
    public boolean agregarAuxVenta(String idarticulo, String idventa, String cantidad, String total){
        try {
            String sentencia="INSERT INTO mercadolibre.auxproductoventa values(?,?,?,?);";
            PreparedStatement stm=cone.prepareStatement(sentencia);
            stm.setString(1, idarticulo);
            stm.setString(2, idventa);
            stm.setString(3, cantidad);
            stm.setString(4, total);
            stm.executeUpdate();
            stm.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
