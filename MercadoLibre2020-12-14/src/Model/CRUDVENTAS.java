
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
/**
 *
 * @author Ismael
 */
public class CRUDVENTAS extends Conexion{
    public boolean agregarVenta(String idVenta, String idusuario, String fecha, String totalVenta){
       try {
            String sentencia="INSERT INTO mercado.usuario values(null,?,?,?);";

            PreparedStatement stm=cone.prepareStatement(sentencia);
            stm.setString(1, idVenta);
            stm.setString(2, idusuario);
            stm.setString(3, fecha);
            stm.setString(4, totalVenta);
            
            stm.executeUpdate();
            stm.close();            
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }     
    }
    public String[][] verVentas(){
        String datos[][]=null;
        String sentencia="SELECT ventas.idventa, usuarios.idusuario,usuarios.nombre, fecha, total_venta FROM ventas, usuarios where ventas.idusuario = usuarios.idusuario ";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            rs=ps.executeQuery();
            int renglones = 0;
            while(rs.next()){
                renglones++;          
            }
            datos = new String[renglones][5];
                rs.first();
            int i = 0;
            do {                
                datos[i][0]=rs.getString(1);
                datos[i][1]=rs.getString(2);
                datos[i][2]=rs.getString(3);
                datos[i][3]=rs.getString(4);
                datos[i][4]=rs.getString(5);
                i++;
            } while (rs.next());
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
    
    public boolean modificarVentas(String idv, String idu, String nombre, String fecha, int total){
        try {
            PreparedStatement ps = cone.prepareStatement("UPDATE mercadolibre.ventas, usuarios set usuarios.idusuario=?,ventas.idventa =?,  usuarios.nombre = ?, fecha = ?, \n" +
                "total_venta=? where ventas.idventa = usuarios.idusuario");
            ps.setString(1, idu);
            ps.setString(2, nombre);
            ps.setString(3, fecha);          
            ps.setInt(4, total);
            ps.setString(5, idv);         
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
    }
     public String[][] verVentasLike(String nombre){
        String datos[][]=null;
        String sentencia="SELECT * FROM ventas where fecha like ?";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            ps.setString(1,nombre);
            rs=ps.executeQuery();
            int renglones = 0;
            while(rs.next()){
                renglones++;          
            }
            datos = new String[renglones][5];
                rs.first();
            int i = 0;
            do {                
                datos[i][0]=rs.getString(1);
                datos[i][1]=rs.getString(2);
                datos[i][2]=rs.getString(3);
                datos[i][3]=rs.getString(4);
                datos[i][4]=rs.getString(5);
                i++;
            } while (rs.next());
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
}
