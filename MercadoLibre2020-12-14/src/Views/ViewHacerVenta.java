/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.ProductosController;
import Model.CRUD;
import Model.CRUDHacerVenta;
import Model.ColorCeldaHacer;
import Model.ColorCeldaVentas;
import Model.ColorDeCelda;
import Model.ColorDeCeldaProductos;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;



/**
 *
 * @author Jovan Ivan Castañeda
 */
public class ViewHacerVenta extends javax.swing.JFrame {
    ColorCeldaHacer cfilas = new ColorCeldaHacer();
    ArrayList<ProductosController> datosCarrito = new ArrayList<ProductosController>();
    public static String rol="";
    public static String id="";
    public static String nombre="";
    
    /**
     * Creates new form ViewHacerVenta
     */
    public ViewHacerVenta(String id, String nombre, String rol) {
        initComponents();
        UIManager UI=new UIManager();
            UI.put("OptionPane.background",new ColorUIResource(251,198,126));
            UI.put("Panel.background",new ColorUIResource(249,119,0));   
        tlbCarrito.setDefaultRenderer(tlbCarrito.getColumnClass(0), cfilas);
        tlbProductos.setDefaultRenderer(tlbProductos.getColumnClass(0), cfilas);
        txtCliente.setText(nombre);
        txtIdCliente.setText(id);
        this.rol=rol;
        this.id=id;
        this.nombre=nombre;
        actualizarTabla();
        actualizarCarrito();
        horaFecha();
        this.lblTitulo.setBackground(Color.black);
        this.lblTitulo.setOpaque(true);
        getContentPane().setBackground(new Color(8, 25, 43)); // Cambiar el color de fondo
        this.setLocationRelativeTo(null); // Pantalla centrada
        txtPaga.setText("Inserte la cantidad a pagar");
        txtPaga.setForeground(Color.gray);
        txtId.setVisible(false);
        txtIdCliente.setVisible(false);
        txtEntrega.setVisible(false);
        txtPrecio.setVisible(false);
        txtCambio.setForeground(Color.WHITE);
    }
    
    public Icon icono(String path, int width, int heigth){
        Icon img = new ImageIcon(new ImageIcon(getClass().getResource(path)).getImage()
                .getScaledInstance(width, heigth, java.awt.Image.SCALE_SMOOTH));
        return img;
    }
    
    public void horaFecha(){
        Date fecha=new Date(); // Sistema actual La fecha y la hora se asignan a fecha
        System.out.println(fecha);
        String strDateFormat = "yyyy-MM-dd"; // El formato de fecha está especificado  
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); // La cadena de formato de fecha se pasa como un argumento al objeto 
        lblFecha.setText(objSDF.format(fecha));
    }
    
    public void limpiarTexto(){
        txtCantidad.setText("");
        txtEntrega.setText("");
        txtProducto.setText("");
        txtPrecio.setText("");
        txtId.setText("");
    }
    
    public void actualizarTabla(){
        CRUDHacerVenta obj = new CRUDHacerVenta();
        String datos[][]=obj.verVentas();
        tlbProductos.setModel(new javax.swing.table.DefaultTableModel(
            datos,
            new String [] {
                "Id", "Nombre", "Descripcion", "Precio", "Tamaño", "Imagen","Entrega","Estatus"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        //String cabecera[] = {"Id","Nombre","Descripcion","Precio","Tamaño","Imagen","Entrega"};
        //DefaultTableModel modeloTabla=new DefaultTableModel(datos, cabecera);
        //tlbProductos.setModel(modeloTabla);
        DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();
        headerRenderer.setBackground(new Color(32, 136, 203));

        for (int i = 0; i < tlbProductos.getModel().getColumnCount(); i++) {
           tlbProductos.getColumnModel().getColumn(i).setHeaderRenderer(headerRenderer);
        }
          ocultarDatos();
    }
    
    public void ocultarDatos(){
        tlbProductos.getColumnModel().getColumn(0).setMaxWidth(0);
        tlbProductos.getColumnModel().getColumn(0).setMinWidth(0);
        tlbProductos.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        tlbProductos.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        tlbProductos.getColumnModel().getColumn(5).setMaxWidth(0);
        tlbProductos.getColumnModel().getColumn(5).setMinWidth(0);
        tlbProductos.getTableHeader().getColumnModel().getColumn(5).setMaxWidth(0);
        tlbProductos.getTableHeader().getColumnModel().getColumn(5).setMinWidth(0);
    }
    
    public void actualizarCarrito(){
        DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();
        headerRenderer.setBackground(new Color(32, 136, 203));

        for (int i = 0; i < tlbCarrito.getModel().getColumnCount(); i++) {
           tlbCarrito.getColumnModel().getColumn(i).setHeaderRenderer(headerRenderer);
        }
          ocultarid();
    }
    
    public void ocultarid(){
        tlbCarrito.getColumnModel().getColumn(0).setMaxWidth(0);
        tlbCarrito.getColumnModel().getColumn(0).setMinWidth(0);
        tlbCarrito.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        tlbCarrito.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitulo = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtProducto = new javax.swing.JTextField();
        txtCantidad = new javax.swing.JTextField();
        btnAgregar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tlbCarrito = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tlbProductos = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        btnEliminar = new javax.swing.JButton();
        lblFecha = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtPaga = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        txtId = new javax.swing.JTextField();
        txtEntrega = new javax.swing.JTextField();
        btnVenta = new javax.swing.JButton();
        txtCliente = new javax.swing.JTextField();
        txtIdCliente = new javax.swing.JTextField();
        txtCambio = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setIconImage(getIconImage());
        setPreferredSize(new java.awt.Dimension(921, 495));

        lblTitulo.setBackground(new java.awt.Color(0, 0, 0));
        lblTitulo.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(249, 119, 0));
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("Hacer Venta");
        lblTitulo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir_norm.png"))); // NOI18N
        btnCancelar.setBorderPainted(false);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setFocusPainted(false);
        btnCancelar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir_press.png"))); // NOI18N
        btnCancelar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir_roll.png"))); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(251, 198, 126));
        jLabel1.setText("Cliente:");

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(251, 198, 126));
        jLabel3.setText("Producto:");

        jLabel4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(251, 198, 126));
        jLabel4.setText("Cantidad:");

        txtProducto.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtProducto.setEnabled(false);
        txtProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtProductoActionPerformed(evt);
            }
        });

        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCantidadKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadKeyTyped(evt);
            }
        });

        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add_norm.png"))); // NOI18N
        btnAgregar.setBorderPainted(false);
        btnAgregar.setContentAreaFilled(false);
        btnAgregar.setFocusPainted(false);
        btnAgregar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add_press.png"))); // NOI18N
        btnAgregar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add_roll_rj.png"))); // NOI18N
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(251, 198, 126));
        jLabel5.setText("Buscar");

        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        tlbCarrito.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Nombre", "Precio", "Cantidad", "Total", "Entrega"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tlbCarrito);

        tlbProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tlbProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tlbProductosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tlbProductos);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(251, 198, 126));
        jLabel6.setText("Productos Agregados");

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar_norm.png"))); // NOI18N
        btnEliminar.setBorderPainted(false);
        btnEliminar.setContentAreaFilled(false);
        btnEliminar.setFocusPainted(false);
        btnEliminar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar_press.png"))); // NOI18N
        btnEliminar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar_roll.png"))); // NOI18N
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        lblFecha.setForeground(new java.awt.Color(255, 255, 255));
        lblFecha.setText("Fecha");

        jLabel8.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(251, 198, 126));
        jLabel8.setText("Total de la venta:");

        lblTotal.setForeground(new java.awt.Color(0, 255, 0));
        lblTotal.setText("0");

        jLabel9.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(251, 198, 126));
        jLabel9.setText("Cantidad Pagada:");

        jLabel10.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(251, 198, 126));
        jLabel10.setText("Cambio:");

        txtPaga.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPagaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPagaFocusLost(evt);
            }
        });
        txtPaga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagaActionPerformed(evt);
            }
        });
        txtPaga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPagaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPagaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPagaKeyTyped(evt);
            }
        });

        txtPrecio.setEnabled(false);

        txtId.setEnabled(false);

        txtEntrega.setEnabled(false);

        btnVenta.setText("Agregar Venta");
        btnVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVentaActionPerformed(evt);
            }
        });

        txtCliente.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtCliente.setEnabled(false);

        txtIdCliente.setEnabled(false);

        txtCambio.setText("0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel1))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane2))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(16, 16, 16))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 445, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(jLabel9)
                                                    .addComponent(jLabel8)
                                                    .addComponent(jLabel10))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(btnVenta)
                                                    .addComponent(lblTotal)
                                                    .addComponent(txtPaga, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(txtCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addComponent(lblFecha)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(jLabel6)))
                                .addGap(0, 24, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(179, 179, 179))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblFecha)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(lblTotal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(txtPaga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel10)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtCambio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(1, 1, 1)))
                        .addComponent(btnVenta))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        CRUDHacerVenta obj = new CRUDHacerVenta();
        String datos[][]=obj.getVentasLike("%"+txtBuscar.getText()+"%");
        tlbProductos.setModel(new javax.swing.table.DefaultTableModel(
            datos,
            new String [] {
                "Id", "Nombre", "Descripcion", "Precio", "Tamaño", "Imagen","Entrega"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        //String cabecera[] = {"Id","Nombre","Descripcion","Precio","Tamaño","Imagen","Entrega"};
        //DefaultTableModel modeloTabla=new DefaultTableModel(datos, cabecera);
        //tlbProductos.setModel(modeloTabla);
        DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();
        headerRenderer.setBackground(new Color(32, 136, 203));

        for (int i = 0; i < tlbProductos.getModel().getColumnCount(); i++) {
           tlbProductos.getColumnModel().getColumn(i).setHeaderRenderer(headerRenderer);
        }
        //ocultarDatos();
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        try{
        String estatus = tlbProductos.getModel().getValueAt(tlbProductos.getSelectedRow(), 7).toString();      
        if(estatus.equals("Habilitado")){
        String texto=txtCantidad.getText();
        texto=texto.replaceAll(" ", "");
        if(texto.length()==0){
            JOptionPane.showMessageDialog(rootPane, "Debe insertar una cantidad para el producto","Mercado Libre", 2);
        }else if(txtEntrega.getText().length()==0){
            JOptionPane.showMessageDialog(rootPane, "Debe seleccionar un producto", "Mercado Libre", 2);
        }else{
            int precio, cantidad, total;
            precio = Integer.parseInt(txtPrecio.getText());
            cantidad = Integer.parseInt(txtCantidad.getText());
            total = precio * cantidad;
            ProductosController productos = new ProductosController(txtId.getText(), txtProducto.getText(), total, txtEntrega.getText(), precio, cantidad);
            datosCarrito.add(productos);
            mostrar();
            limpiarTexto();
        }
       }else{
           JOptionPane.showMessageDialog(rootPane, "No puede realizar una venta porque el producto esta deshabilitado","Mercado Libre",2);
       }
       }catch(Exception e){
           System.out.println(e.toString());
           JOptionPane.showMessageDialog(rootPane, "No puede realizar una venta porque el producto esta deshabilitado");
       }
    }//GEN-LAST:event_btnAgregarActionPerformed

    public void mostrar(){
        String matris[][] = new String[datosCarrito.size()][6];
        int totalVenta = 0;
        String precio, cantidad, total;
        
        for (int i = 0; i < datosCarrito.size(); i++) {
            totalVenta += datosCarrito.get(i).getTotal();
            precio = String.valueOf(datosCarrito.get(i).getPrecio());
            cantidad = String.valueOf(datosCarrito.get(i).getCantidad());
            total = String.valueOf(datosCarrito.get(i).getTotal());
            matris[i][0] = datosCarrito.get(i).getId();
            matris[i][1] = datosCarrito.get(i).getNombre();
            matris[i][2] = precio;
            matris[i][3] = cantidad;
            matris[i][4] = total;
            matris[i][5] = datosCarrito.get(i).getEntrega();
        }
        
        lblTotal.setText(String.valueOf(totalVenta));
        
        tlbCarrito.setModel(new javax.swing.table.DefaultTableModel(
            matris,
            new String [] {
                "Id", "Nombre", "Precio", "Cantidad", "Total", "Entrega"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        
        actualizarCarrito();
    }
    
    /**
     * Metodo para eliminar productos de la tabla productos
     * @param eliminado -> Necesita un parametro para eliminar que en este caso toma el id
     */
    public void mostrarAlEliminar(String eliminado){
        String matris[][] = new String[datosCarrito.size()][6];
        int totalVenta = 0;
        String precio, cantidad, total;
        
        for (int i = 0; i < datosCarrito.size(); i++) {
            if(datosCarrito.get(i).getId().equals(eliminado)){
                datosCarrito.remove(i);
            }
        }
        
        for (int i = 0; i < datosCarrito.size(); i++) {
            totalVenta += datosCarrito.get(i).getTotal();
            precio = String.valueOf(datosCarrito.get(i).getPrecio());
            cantidad = String.valueOf(datosCarrito.get(i).getCantidad());
            total = String.valueOf(datosCarrito.get(i).getTotal());
            matris[i][0] = datosCarrito.get(i).getId();
            matris[i][1] = datosCarrito.get(i).getNombre();
            matris[i][2] = precio;
            matris[i][3] = cantidad;
            matris[i][4] = total;
            matris[i][5] = datosCarrito.get(i).getEntrega();
        }
        
        lblTotal.setText(String.valueOf(totalVenta));
        
        tlbCarrito.setModel(new javax.swing.table.DefaultTableModel(
            matris,
            new String [] {
                "Id", "Nombre", "Precio", "Cantidad", "Total", "Entrega"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        
        actualizarCarrito();
    }
    
    private void tlbProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tlbProductosMouseClicked
        int seleccionar=tlbProductos.rowAtPoint(evt.getPoint());
        txtId.setText(String.valueOf(tlbProductos.getValueAt(seleccionar, 0)));
        txtProducto.setText(String.valueOf(tlbProductos.getValueAt(seleccionar, 1)));
        txtPrecio.setText(String.valueOf(tlbProductos.getValueAt(seleccionar, 3)));
        txtEntrega.setText(String.valueOf(tlbProductos.getValueAt(seleccionar, 6)));
    }//GEN-LAST:event_tlbProductosMouseClicked

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
        ViewPantallaPrincipal principal=new ViewPantallaPrincipal(rol);
        principal.setVisible(true);
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        try {
            String eliminar=tlbCarrito.getModel().getValueAt(tlbCarrito.getSelectedRow(), 0).toString();
            int eleccion = JOptionPane.showConfirmDialog(rootPane, "Estas seguro de querer eliminar este producto de la venta", "¿Quieres eliminar?", 0, 1);
            if (eleccion == 0) {
            mostrarAlEliminar(eliminar);
        }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Debe seleccionar una fila para eliminar");
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void txtPagaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPagaKeyTyped
                char c = evt.getKeyChar();
      if((c<'0' || c>'9') || txtPaga.getText().length()==15) 
          evt.consume();   
        /*try {
            char c = evt.getKeyChar();
            if((c<'0' || c>'9') || txtPrecio.getText().length()==11){
            evt.consume();
            if (txtPrecio.getText().equals("")) {
                System.out.println("ENTRO");
                int total = Integer.parseInt(lblTotal.getText());
                int paga = Integer.parseInt(txtPaga.getText());
                int cambio = total-paga;
                txtCambio.setText(String.valueOf(cambio));
            }
        }
        } catch (NumberFormatException e) {
            System.out.println("Error al convertir");
        }*/
    }//GEN-LAST:event_txtPagaKeyTyped

    private void txtPagaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPagaFocusGained
        if (txtPaga.getText().equals("Inserte la cantidad a pagar")) {
            txtPaga.setText("");
            txtPaga.setForeground(Color.black);
            txtCambio.setText("0");
        }
    }//GEN-LAST:event_txtPagaFocusGained

    private void txtPagaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPagaFocusLost
        if (txtPaga.getText().equals("")) {
            txtPaga.setForeground(Color.gray);
            txtPaga.setText("Inserte la cantidad a pagar");
            txtCambio.setText("0");
        }
    }//GEN-LAST:event_txtPagaFocusLost

    private void txtPagaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPagaKeyPressed
        /*char c = evt.getKeyChar();
        if((c<'0' || c>'9') || txtPaga.getText().length()==11){
            evt.consume();
        }*/
    }//GEN-LAST:event_txtPagaKeyPressed

    private void txtPagaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPagaKeyReleased
        String numero=txtPaga.getText();
        if (numero.equals("")==false&&numero.matches("[0-9]*")) {
            int total = Integer.parseInt(lblTotal.getText());
            int paga = Integer.parseInt(txtPaga.getText());
            int cambio = paga-total;
            txtCambio.setText(String.valueOf(cambio+(paga-paga)));
            if(cambio < 0){
                txtCambio.setForeground(Color.red);
                //System.out.println("Cambia de color rojo");
            }else{
                txtCambio.setForeground(Color.YELLOW);
                //System.out.println("Cambia de color verde");
            }
        }else{
            txtCambio.setText("0");
        }
    }//GEN-LAST:event_txtPagaKeyReleased

    private void btnVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVentaActionPerformed
              if (datosCarrito.isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Debe insertar productos para hacer una compra","Mercado libre",2);
        }else if(txtPaga.getText().equals("")){
            JOptionPane.showMessageDialog(rootPane, "Debe agregar una cantidad de pago","Mercado libre",2);
        }else if(txtPaga.getText().equals("Inserte la cantidad a pagar")){
            JOptionPane.showMessageDialog(rootPane, "Debe agregar una cantidad de pago","Mercado libre",2);
        }else{
            CRUDHacerVenta venta = new CRUDHacerVenta();
            venta.agregarVenta(txtIdCliente.getText(), lblFecha.getText(), lblTotal.getText(), txtPaga.getText(), txtCambio.getText());
            System.out.println("Hizo la venta");
            String idventa = venta.verVentasId();
            String cantidad, total;
            System.out.println("Empieza insertar los productos");
            for (int i = 0; i < datosCarrito.size(); i++) {
                cantidad = String.valueOf(datosCarrito.get(i).getCantidad());
                total = String.valueOf(datosCarrito.get(i).getTotal());
                venta.agregarAuxVenta(datosCarrito.get(i).getId(), idventa, cantidad, total);
            }
            JOptionPane.showMessageDialog(rootPane, "Se realizo la venta","Mercado libre", JOptionPane.PLAIN_MESSAGE, 
                    icono("/imagenes/comprobado.png",64,64));
        }
       
    }//GEN-LAST:event_btnVentaActionPerformed

    private void txtCantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyReleased
       
    }//GEN-LAST:event_txtCantidadKeyReleased

    private void txtCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyTyped
    char c = evt.getKeyChar();
      if((c<'0' || c>'9') || txtCantidad.getText().length()==15) 
          evt.consume();   
    }//GEN-LAST:event_txtCantidadKeyTyped

    private void txtProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtProductoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtProductoActionPerformed

    private void txtPagaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewHacerVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewHacerVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewHacerVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewHacerVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewHacerVenta(id, nombre, rol).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnVenta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JTable tlbCarrito;
    private javax.swing.JTable tlbProductos;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JLabel txtCambio;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtEntrega;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtIdCliente;
    private javax.swing.JTextField txtPaga;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtProducto;
    // End of variables declaration//GEN-END:variables
}
