
package Views;


import Model.ColorDeCelda;
import Model.CRUD;
import Model.CRUDUSUARIOS;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.plaf.ColorUIResource;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Ismael
 */
public class ViewUser extends javax.swing.JFrame {
    CRUDUSUARIOS obj;
    UIManager UI;
    public static String rol="";
    /**
     * Creates new form ViewUser
     */
    ColorDeCelda cfilas = new ColorDeCelda();
    public ViewUser(String rol) {
        this.rol=rol;
        initComponents();       
        actualizarTabla();
        UIManager UI=new UIManager();
            UI.put("OptionPane.background",new ColorUIResource(251,198,126));
            UI.put("Panel.background",new ColorUIResource(249,119,0));   
        tabla.setDefaultRenderer(tabla.getColumnClass(10), cfilas);
        tabla.setBackground(Color.yellow);
        this.lblMostrar.setBackground(Color.black);
        this.lblMostrar.setOpaque(true);
        this.getContentPane().setBackground(new Color(8, 25, 43));
        this.setLocationRelativeTo(null);
    }
    
    @Override
    public Image getIconImage(){
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("imagenes/logoM.png"));
        return retValue;
    }
    
    public void actualizarTabla(){
        obj = new CRUDUSUARIOS();
        String datos[][] = obj.verUsuario();
        
        String cabeceras[] = {"Id","Nombre","Calle","CP","Numero","Colonia","Estado","Pais","Imagen","Correo","Estatus"};
        DefaultTableModel modeloTabla = new DefaultTableModel(datos,cabeceras);
        tabla.setModel(modeloTabla);
        OcultarId();
    }
    public void OcultarId(){
        tabla.getColumnModel().getColumn(0).setMaxWidth(0);
        tabla.getColumnModel().getColumn(0).setMinWidth(0);
        tabla.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        tabla.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        }
    public void BotonHabilitado(){
        
    }
    public Icon icono(String path, int width, int heigth){
        Icon img = new ImageIcon(new ImageIcon(getClass().getResource(path)).getImage()
                .getScaledInstance(width, heigth, java.awt.Image.SCALE_SMOOTH));
        return img;
    }
    public void Bloqueo(){       
        String estatus = tabla.getModel().getValueAt(tabla.getSelectedRow(), 10).toString();
        if(estatus.equals("Alta")){
            btnModificar.setEnabled(true);           
        }else{
            btnModificar.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        txtBuscar = new javax.swing.JTextField();
        btnModificar = new javax.swing.JButton();
        lblMostrar = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnAgregar = new javax.swing.JButton();
        txtSalir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnExportar1 = new javax.swing.JButton();
        boxSelect = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();

        jMenuItem1.setText("jMenuItem1");

        jMenuItem2.setText("jMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mostrar el registro de usuarios");
        setIconImage(getIconImage());

        tabla.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabla.setAutoscrolls(false);
        tabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabla);

        txtBuscar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        btnModificar.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/editar_norm.png"))); // NOI18N
        btnModificar.setBorderPainted(false);
        btnModificar.setContentAreaFilled(false);
        btnModificar.setFocusPainted(false);
        btnModificar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/editar_press.png"))); // NOI18N
        btnModificar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/editar_roll.png"))); // NOI18N
        btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnModificarMouseClicked(evt);
            }
        });
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        lblMostrar.setBackground(Color.BLACK);
        lblMostrar.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lblMostrar.setForeground(new java.awt.Color(249, 119, 0));
        lblMostrar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMostrar.setText("Mostrar Registro de Usuarios");

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(251, 198, 126));
        jLabel5.setText("Buscar:");

        btnAgregar.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        btnAgregar.setForeground(new java.awt.Color(255, 255, 255));
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo_norm.png"))); // NOI18N
        btnAgregar.setBorderPainted(false);
        btnAgregar.setContentAreaFilled(false);
        btnAgregar.setFocusPainted(false);
        btnAgregar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgregar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo_press.png"))); // NOI18N
        btnAgregar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo_roll.png"))); // NOI18N
        btnAgregar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAgregarMouseClicked(evt);
            }
        });
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        txtSalir.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtSalir.setForeground(new java.awt.Color(255, 255, 255));
        txtSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir_norm.png"))); // NOI18N
        txtSalir.setBorderPainted(false);
        txtSalir.setContentAreaFilled(false);
        txtSalir.setFocusPainted(false);
        txtSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        txtSalir.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir_press.png"))); // NOI18N
        txtSalir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir_roll.png"))); // NOI18N
        txtSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtSalirMouseClicked(evt);
            }
        });
        txtSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSalirActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 0, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnExportar1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        btnExportar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/export_pdf_48px.png"))); // NOI18N
        btnExportar1.setContentAreaFilled(false);
        btnExportar1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/export_pdf_96px.png"))); // NOI18N
        btnExportar1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExportar1MouseClicked(evt);
            }
        });
        btnExportar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportar1ActionPerformed(evt);
            }
        });

        boxSelect.setBackground(new java.awt.Color(251, 198, 126));
        boxSelect.setForeground(new java.awt.Color(251, 198, 126));
        boxSelect.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Estado", "Correo" }));

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(251, 198, 126));
        jLabel4.setText("Crear Reporte:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnExportar1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(boxSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(93, 93, 93))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(boxSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnExportar1)))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel_48px.png"))); // NOI18N
        btnSalir.setContentAreaFilled(false);
        btnSalir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel_48px1.png"))); // NOI18N
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblMostrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 393, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(38, 38, 38)
                                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(109, 109, 109)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(56, 56, 56)))
                        .addGap(226, 226, 226)
                        .addComponent(btnSalir)
                        .addGap(89, 89, 89)))
                .addGap(34, 34, 34))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(17, 17, 17))
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnSalir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                .addGap(69, 69, 69))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        String datos[][] = obj.verUsuarioLike("%"+txtBuscar.getText()+"%");
        String cabeceras[] = {"Id","Nombre","Calle","CP","Numero","Colonia","Estado","Pais","Imagen","Correo"," Estatus"};
        DefaultTableModel modeloTabla = new DefaultTableModel(datos,cabeceras);
        tabla.setModel(modeloTabla);
        
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnModificarMouseClicked

        if (rol.equals("administrador")) {
            try{            
                String id = tabla.getModel().getValueAt(tabla.getSelectedRow(), 0).toString();
                String Nombre = tabla.getModel().getValueAt(tabla.getSelectedRow(), 1).toString();
                String Calle = tabla.getModel().getValueAt(tabla.getSelectedRow(), 2).toString();
                String Cp = tabla.getModel().getValueAt(tabla.getSelectedRow(), 3).toString();
                String Numero = tabla.getModel().getValueAt(tabla.getSelectedRow(), 4).toString();
                String Colonia = tabla.getModel().getValueAt(tabla.getSelectedRow(), 5).toString();
                String Estado = tabla.getModel().getValueAt(tabla.getSelectedRow(), 6).toString();
                String Pais = tabla.getModel().getValueAt(tabla.getSelectedRow(), 7).toString();
                String Imagen = tabla.getModel().getValueAt(tabla.getSelectedRow(), 8).toString();
                String Correo = tabla.getModel().getValueAt(tabla.getSelectedRow(), 9).toString();
            
                UpdateUser mostrar = new UpdateUser(id, Nombre, Calle, Cp, Numero, Colonia, Estado, Pais, Imagen, Correo, rol);
                mostrar.setVisible(true);
                this.setVisible(false); 
            }catch(Exception e){
                JOptionPane.showMessageDialog(rootPane,"Primero selecciona el usuario que va a modificar","Mercado Libre",
                JOptionPane.PLAIN_MESSAGE, icono("/imagenes/logoM.png",100,100));
            } 
        }else{
           JOptionPane.showMessageDialog(rootPane,"Solo el administrador puede modificar los usuarios","Mercado Libre",0);
        }            
    }//GEN-LAST:event_btnModificarMouseClicked

    private void tablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMouseClicked

                    
    }//GEN-LAST:event_tablaMouseClicked

    private void btnAgregarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAgregarMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAgregarMouseClicked

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        AddUser agregar = new AddUser(rol);
        agregar.setVisible(true);
        this.setVisible(false);  
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void txtSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtSalirMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSalirMouseClicked

    private void txtSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSalirActionPerformed
        this.dispose();
        ViewPantallaPrincipal p = new ViewPantallaPrincipal(rol);
        p.setVisible(true);
    }//GEN-LAST:event_txtSalirActionPerformed

    private void btnExportar1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExportar1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnExportar1MouseClicked

    private void btnExportar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportar1ActionPerformed
        if (rol.equals("administrador")) { 
        SeleccionarN();
        SeleccionarE();
        SeleccionarC();
        }else{
            JOptionPane.showMessageDialog(rootPane,"Solo el administrador puede generar los reportes de los usuarios","Mercado Libre",0); 
        }
    }//GEN-LAST:event_btnExportar1ActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSalirActionPerformed
    
    void SeleccionarN(){
        if(boxSelect.getSelectedItem().toString().equals("Nombre")){
            try{
            Model.Conexion con = new Model.Conexion();
        String ruta = System.getProperty("user.dir")+"/src/Controller/ReporteUsuarios.jasper";          
        Map parametro = new HashMap();
        UIManager UI=new UIManager();
            UI.put("OptionPane.background",new ColorUIResource(8, 25, 43));
            UI.put("Panel.background",new ColorUIResource(8, 25, 43));
        String nombre = JOptionPane.showInputDialog("Inserte un filtro de busqueda");
        parametro.put("pNombre","%"+nombre+"%");
        JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
        JasperPrint jp = JasperFillManager.fillReport(jr,parametro,con.cone);
        JasperViewer jv = new JasperViewer(jp, false);
        jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        jv.setVisible(true);
        }catch(Exception e){
            System.out.println(e.toString());
        }
    }            
    }
    void SeleccionarE(){
        if(boxSelect.getSelectedItem().toString().equals("Estado")){
            try{
            Model.Conexion con = new Model.Conexion();
        String ruta = System.getProperty("user.dir")+"/src/Controller/ReporteUsuarios2.jasper";          
        Map parametro = new HashMap();
        UIManager UI=new UIManager();
            UI.put("OptionPane.background",new ColorUIResource(8, 25, 43));
            UI.put("Panel.background",new ColorUIResource(8, 25, 43));
        String Estado = JOptionPane.showInputDialog("Inserte un filtro de busqueda");
        parametro.put("pEstado","%"+Estado+"%");
        JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
        JasperPrint jp = JasperFillManager.fillReport(jr,parametro,con.cone);
        JasperViewer jv = new JasperViewer(jp, false);
        jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        jv.setVisible(true);
        }catch(Exception e){
            System.out.println(e.toString());
        }
        }
    }
    void SeleccionarC(){
        if(boxSelect.getSelectedItem().toString().equals("Correo")){
            try{
            Model.Conexion con = new Model.Conexion();
        String ruta = System.getProperty("user.dir")+"/src/Controller/ReproteUsuarios3.jasper";          
        Map parametro = new HashMap();
        UIManager UI=new UIManager();
            UI.put("OptionPane.background",new ColorUIResource(8, 25, 43));
            UI.put("Panel.background",new ColorUIResource(8, 25, 43));
        String Correo = JOptionPane.showInputDialog("Inserte un filtro de busqueda");
        parametro.put("pCorreo","%"+Correo+"%");
        JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
        JasperPrint jp = JasperFillManager.fillReport(jr,parametro,con.cone);
        JasperViewer jv = new JasperViewer(jp, false);
        jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        jv.setVisible(true);
        }catch(Exception e){
            System.out.println(e.toString());
        }
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewUser(rol).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> boxSelect;
    public javax.swing.JButton btnAgregar;
    public javax.swing.JButton btnExportar1;
    public javax.swing.JButton btnModificar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblMostrar;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtBuscar;
    public javax.swing.JButton txtSalir;
    // End of variables declaration//GEN-END:variables
}
