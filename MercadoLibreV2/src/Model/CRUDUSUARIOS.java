
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Ismael
 */
public class CRUDUSUARIOS extends Conexion{
    /**
     * 
     * @param nombre ->Nombre(s) y apellidos del usuario.
     * @param calle ->Calle donde recide el usuario.
     * @param cp ->Codigo postal en donde reside el usuario
     * @param numero ->Numero de casa donde recide el usuario.
     * @param colonia ->Coloniia donde recide el usuario
     * @param estado ->Municipio donde recide el usuario.
     * @param pais ->Pais donde recide el usuario.
     * @param imagen ->Ruta de la imagen del usuario.
     * @param correo ->Correo del usuario.
     * @param Estatus->Identifica si un Usuario esta de alta o de Baja.
     * @return 
     */
    public boolean agregarUsuario(String nombre, String calle, String cp, String numero, String colonia, String estado, String pais,
            String imagen, String correo, String Estatus){
       try {
            String sentencia="INSERT INTO mercado.usuario values(null,?,?,?,?,?,?,?,?,?,?);";
            /*String sentencia2="INSERT INTO mercado.paises values(null,?);";
            String sentencia3="INSERT INTO mercado.estados values(null,?);";*/
            PreparedStatement stm=cone.prepareStatement(sentencia);
            /*PreparedStatement stm2=cone.prepareStatement(sentencia2);
            PreparedStatement stm3=cone.prepareStatement(sentencia3);*/
            stm.setString(1, nombre);
            stm.setString(2, calle);
            stm.setString(3, cp);
            stm.setString(4, numero);
            stm.setString(5, colonia);
            stm.setString(6, estado);
            stm.setString(7, pais);
            stm.setString(8, imagen);
            stm.setString(9, correo);
            stm.setString(10, Estatus);
            stm.executeUpdate();
            stm.close();            
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }     
    }
    /**
     * 
     * @param id -> identificador unico para saber que usuario se eliminara
     * @return True: si se hizo correcatamente el eliminado del usuario 
     *         False: si no se elimino correctamente el usuario
     */
        public boolean eliminarUsuario(String id){
           try {
            PreparedStatement ps = cone.prepareStatement("DELETE FROM mercado.usuario WHERE idusuario=?;");
            ps.setString(1, id);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        } 
    }    
        public boolean modificarUsuario(String id, String nombre, String calle, String cp, String numero, String colonia,
              String estado, String pais, String imagen, String correo){
        try {
            Statement ps = cone.createStatement();
            String sentencia = "UPDATE mercado.usuario SET nombre='"+nombre+"',calle='"+calle+"',cp='"+cp+"',numero='"+numero+""
                    + "',colonia='"+colonia+"',estado='"+estado+"',pais='"+pais+"',imagen='"+imagen+"',correo='"+correo+""
                    + "' where idusuario='"+id;
            ps.executeUpdate(sentencia);
            System.out.println("Se modifico correctamente");
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        }
        }
        public boolean modificarUsuario2(String id, String nombre, String calle, String cp, String numero, String colonia,
            String estado, String pais, String imagen, String correo){
        try {
            PreparedStatement ps = cone.prepareStatement("UPDATE mercado.usuario set nombre=?, calle=?, cp=?, numero=?, colonia=?,"
                    + "estado=?, pais=?, imagen=?, correo=? where usuario.idusuario=?;");
            ps.setString(1, nombre);
            ps.setString(2, calle);
            ps.setString(3, cp);
            ps.setString(4, numero);
            ps.setString(5, colonia);
            ps.setString(6, estado);
            ps.setString(7, pais);
            ps.setString(8, imagen);
            ps.setString(9, correo);
            ps.setString(10, id);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
    
    }
        public String[][] verUsuario(){
        String datos[][]=null;
        String sentencia="SELECT * FROM mercado.usuario";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            rs=ps.executeQuery();
            int renglones = 0;
            while(rs.next()){
                renglones++;          
            }
            datos = new String[renglones][11];
                rs.first();
            int i = 0;
            do {                
                datos[i][0]=rs.getString(1);
                datos[i][1]=rs.getString(2);
                datos[i][2]=rs.getString(3);
                datos[i][3]=rs.getString(4);
                datos[i][4]=rs.getString(5);
                datos[i][5]=rs.getString(6);
                datos[i][6]=rs.getString(7);
                datos[i][7]=rs.getString(8);
                datos[i][8]=rs.getString(9);
                datos[i][9]=rs.getString(10);
                datos[i][10]=rs.getString(11);
                i++;
            } while (rs.next());
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
        public String[][] verUsuarioLike(String nombre){
        String datos[][]=null;
        String sentencia="SELECT * FROM mercado.usuario where nombre like ?";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            ps.setString(1,nombre);
            rs=ps.executeQuery();
            int renglones = 0;
            while(rs.next()){
                renglones++;          
            }
            datos = new String[renglones][11];
                rs.first();
            int i = 0;
            do {                
                datos[i][0]=rs.getString(1);
                datos[i][1]=rs.getString(2);
                datos[i][2]=rs.getString(3);
                datos[i][3]=rs.getString(4);
                datos[i][4]=rs.getString(5);
                datos[i][5]=rs.getString(6);
                datos[i][6]=rs.getString(7);
                datos[i][7]=rs.getString(8);
                datos[i][8]=rs.getString(9);
                datos[i][9]=rs.getString(10);
                datos[i][10]=rs.getString(11);
                i++;
            } while (rs.next());
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
    public String[][] getPaises(String sentencia){
        String datos[][]=null;
        try{
            PreparedStatement ps = cone.prepareStatement(sentencia, ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            ResultSet rs=ps.executeQuery();
            int ren=0;
            while (rs.next()){
                ren++;
            }
            rs.first();
            datos=new String[ren][2];
            int i = 0;
            do{
                datos[i][0]=rs.getString(1);
                datos[i][0]=rs.getString(2);
                i++;
            }while (rs.next());         
        }catch (Exception e){
            System.out.println(e.toString());
            System.out.println("No se pasan we :(");
        }
        return datos;
    }
    public String[][] getEstados(String sentencia){
        String datos[][]=null;
        try{
            PreparedStatement ps = cone.prepareStatement(sentencia, ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            ResultSet rs=ps.executeQuery();
            int ren=0;
            while (rs.next()){
                ren++;
            }
            rs.first();
            datos=new String[ren][2];
            int i = 0;
            do{
                datos[i][0]=rs.getString(1);
                datos[i][0]=rs.getString(2);
                i++;
            }while (rs.next());         
        }catch (Exception e){
            System.out.println(e.toString());
        }
        return datos;
    } 
    public boolean BajaUsuario(String id){
        try {
            PreparedStatement ps = cone.prepareStatement("UPDATE mercado.usuario SET Estatus = 'Baja' where idusuario=?");
             ps.setString(1, id);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
    }
    public boolean AltaUsuario(String id){
        try {
            PreparedStatement ps = cone.prepareStatement("UPDATE mercado.usuario set Estatus = 'Alta' where usuario.idusuario=?;");
             ps.setString(1, id);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
    }
}