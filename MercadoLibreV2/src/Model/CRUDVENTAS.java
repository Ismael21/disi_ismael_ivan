
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
/**
 *
 * @author Ismael
 */
public class CRUDVENTAS extends Conexion{
    public boolean agregarVenta(String idVenta, String idusuario, String fecha, String totalVenta){
       try {
            String sentencia="INSERT INTO mercado.venta values(null,?,?,?);";

            PreparedStatement stm=cone.prepareStatement(sentencia);
            stm.setString(1, idVenta);
            stm.setString(2, idusuario);
            stm.setString(3, fecha);
            stm.setString(4, totalVenta);
            
            stm.executeUpdate();
            stm.close();            
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }     
    }
    public String[][] verVentas(){
        String datos[][]=null;
        String sentencia="SELECT venta.idventa, usuario.idusuario, fecha, total_venta FROM venta, usuario \n" +
 "where venta.idusuario = usuario.idusuario";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            rs=ps.executeQuery();
            int renglones = 0;
            while(rs.next()){
                renglones++;          
            }
            datos = new String[renglones][4];
                rs.first();
            int i = 0;
            do {                
                datos[i][0]=rs.getString(1);
                datos[i][1]=rs.getString(2);
                datos[i][2]=rs.getString(3);
                datos[i][3]=rs.getString(4);                
                i++;
            } while (rs.next());
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
    public boolean modificarVentas(String idv, String idu, String fecha, int total){
        try {
            PreparedStatement ps = cone.prepareStatement("UPDATE mercado.venta set idusuario=?, fecha = ?, \n" +
"total_venta=? where venta.idventa=?;");
            ps.setString(1, idu);
            ps.setString(2, fecha);
            ps.setInt(3, total);
            ps.setString(4, idv);         
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
}
}
