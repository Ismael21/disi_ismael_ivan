package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class CRUD extends Conexion{
    
    /**
     * Metodo para agregar productos
     * @param nombre -> Nombre del usuario
     * @param descripcion -> Descripcion del producto
     * @param precio -> Precio del producto
     * @param tamaño -> Tamaño del producto
     * @param imagen -> Ruta de acceso de la imagen
     * @param estatus -> Habilitar o desabilitar producto
     * @param entrega -> Define si se entrega ese mismo dia o no
     * @return True: si se hizo correcatamente el agregado del producto 
     *         False: si no se agrego correctamente el producto
     */
    public boolean agregarProducto(String nombre, String descripcion, int precio, String tamaño, String imagen, String estatus, String entrega){
        try {
            String sentencia="INSERT INTO mercado.articulos values(null,?,?,?,?,?,?,?);";
            PreparedStatement stm=cone.prepareStatement(sentencia);
            stm.setString(1, nombre);
            stm.setString(2, descripcion);
            stm.setInt(3, precio);
            stm.setString(4, tamaño);
            stm.setString(5, imagen);
            stm.setString(6, estatus);
            stm.setString(7, entrega);
            stm.executeUpdate();
            stm.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
       /*public boolean agregarPais(String Nombre){
       try {
            String sentencia="INSERT INTO mercado.paises values(null,?);";
            PreparedStatement stm=cone.prepareStatement(sentencia);
            stm.setString(1, Nombre);
            stm.executeUpdate();
            stm.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
       public boolean agregarEstado(String nombre){
       try {
            String sentencia="INSERT INTO mercado.paises values(null,?);";
            PreparedStatement stm=cone.prepareStatement(sentencia);
            stm.setString(1, nombre);
            stm.executeUpdate();
            stm.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    } */
    
    /**
     * 
     * @param id -> identificador unico para saber que producto se eliminara
     * @return True: si se hizo correcatamente el eliminado del producto 
     *         False: si no se elimino correctamente el producto
     */
    public boolean eliminarProducto(String id){
        try {
            PreparedStatement ps = cone.prepareStatement("DELETE FROM mercado.articulos WHERE idarticulo=?;");
            ps.setString(1, id);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
    }
    
                
    /**
     * 
     * @param id
     * @param nombre
     * @param descripcion
     * @param precio
     * @param tamaño
     * @param imagen
     * @param estatus
     * @param entrega
     * @return 
     */
    public boolean modificarProducto(String id, String nombre, String descripcion, int precio, String tamaño, String imagen,
            String estatus, String entrega){
        try {
            PreparedStatement ps = cone.prepareStatement("UPDATE mercado.articulos set nombre=?, descripcion=?, precio=?, tamaño=?, imagen=?,"
                    + "estatus=?, entrega=? where idarticulo=?;");
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setInt(3, precio);
            ps.setString(4, tamaño);
            ps.setString(5, imagen);
            ps.setString(6, estatus);
            ps.setString(7, entrega);
            ps.setString(8, id);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
    }
     
    
    /**
     * 
     * @return 
     */
    public String[][] verProductos(){
        String datos[][]=null;
        String sentencia="SELECT * FROM mercado.articulos";
        ResultSet rs=null;
        try {
            PreparedStatement ps=cone.prepareStatement(sentencia,  ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
            rs=ps.executeQuery();
            int renglones = 0;
            while(rs.next()){
                renglones++;
            }
            datos = new String[renglones][8];
                rs.first();
            int i = 0;
            do {                
                datos[i][0]=rs.getString(1);
                datos[i][1]=rs.getString(2);
                datos[i][2]=rs.getString(3);
                datos[i][3]=rs.getString(4);
                datos[i][4]=rs.getString(5);
                datos[i][5]=rs.getString(6);
                datos[i][6]=rs.getString(7);
                datos[i][7]=rs.getString(8);
                i++;
            } while (rs.next());
        } catch (Exception e) {
            System.out.println("Error al mostrar");
            System.out.println(e.toString());
        }
        return datos;
    }
    
}


